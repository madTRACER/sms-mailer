#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "database.h"
#include "connection.h"
#include "filepath.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void showTables();

private slots:
    void on_connectModem_triggered();

    void on_parseRegistry_triggered();

    void on_mailing_triggered();

private:
    Ui::MainWindow *ui;
    QString _portNum;
};

#endif // MAINWINDOW_H
