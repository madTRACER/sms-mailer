#include "filepath.h"
#include "ui_filepath.h"

FilePath::FilePath(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FilePath)
{
    ui->setupUi(this);
}

FilePath::~FilePath()
{
    delete ui;
}

void FilePath::on_pathButton_clicked()
{
    _path = QFileDialog::getOpenFileName();
    ui->pathLine->setText(_path);
}

void FilePath::on_uploadButton_clicked()
{
    Database registry;
    registry.setDatabase("clients", "127.0.0.1", "postgres", "yodick");
    registry.uploadTable(_path);
}
