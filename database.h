#ifndef DATABASE_H
#define DATABASE_H

#include <QSqlDatabase>
#include <QSqlTableModel>
#include <QTableView>
#include <QSqlQuery>

class Database
{
public:
    Database();
    QSqlTableModel* getTable(QString tableName);
    void setDatabase(QString dbName, QString hostName, QString userName, QString password);
    void uploadTable(QString path);

private:

protected:

};

#endif // DATABASE_H

